import React, { Component } from "react";
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableOpacity,
  Image
} from "react-native";
import Logo from "../images/sport.png";

export default class Register extends Component {
  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.logo}>Main Bersama</Text>
        <Image source={Logo} style={{ height: "25%", width: "80%" }} />
        <View style={styles.inputView}>
          <TextInput
            secureTextEntry
            style={styles.inputText}
            placeholder="Name"
            placeholderTextColor="#718093"
            onChangeText={text => {
              // this.setState({name: text})
            }}
          />
        </View>
        <View style={styles.inputView}>
          <TextInput
            secureTextEntry
            style={styles.inputText}
            placeholder="Username"
            placeholderTextColor="#718093"
            onChangeText={text => {}}
          />
        </View>
        <View style={styles.inputView}>
          <TextInput
            secureTextEntry
            style={styles.inputText}
            placeholder="Email"
            placeholderTextColor="#718093"
            onChangeText={text => {}}
          />
        </View>
        <View style={styles.inputView}>
          <TextInput
            secureTextEntry
            style={styles.inputText}
            placeholder="Password"
            placeholderTextColor="#718093"
            onChangeText={text => {}}
          />
        </View>
        <TouchableOpacity
          style={styles.signupBtn}
          onPress={() => this.props.navigation.navigate("Login")}
        >
          <Text style={styles.loginText}>Register</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#ffffff",
    alignItems: "center",
    justifyContent: "center"
  },
  logo: {
    fontWeight: "bold",
    fontSize: 40,
    color: "#fb5b5a",
    marginBottom: 5
  },
  inputView: {
    width: "80%",
    backgroundColor: "#F5F6FA",
    borderRadius: 25,
    height: 50,
    marginBottom: 10,
    justifyContent: "center",
    padding: 20,
    shadowColor: "black",
    shadowOpacity: 0.2,
    shadowRadius: 10,
    shadowOffset: { height: 4, width: 4 }
  },
  inputText: {
    height: 50,
    color: "#718093"
  },
  forgot: {
    color: "#718093",
    fontSize: 11
  },
  loginBtn: {
    width: "80%",
    backgroundColor: "#273C75",
    borderRadius: 25,
    height: 50,
    alignItems: "center",
    justifyContent: "center",
    marginTop: 40,
    marginBottom: 10
  },
  loginText: {
    color: "white"
  },
  signupBtn: {
    width: "80%",
    backgroundColor: "#F6B93B",
    borderRadius: 25,
    height: 50,
    alignItems: "center",
    justifyContent: "center",
    marginTop: 15,
    marginBottom: 10
  }
});

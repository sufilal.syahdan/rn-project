import React, { Component } from "react";
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableOpacity,
  Image,
  ScrollView,
  AsyncStorage
} from "react-native";
import { Ionicons } from "@expo/vector-icons";
import Logo from "../images/sport.png";

export default class Dashboard extends Component {
  // const _retrieveData = async () => {
  //   try {
  //     const value = await AsyncStorage.getItem("TASKS");
  //     if (value !== null) {
  //       // We have data!!
  //       console.log(value);
  //     }
  //   } catch (error) {
  //     // Error retrieving data
  //   }
  // };
  render() {
    return (
      <View style={styles.screen}>
        <ScrollView contentContainerStyle={styles.container}>
          <View style={styles.searchRow}>
            <View style={styles.searchInput}>
              <TextInput
                style={styles.inputText}
                placeholder="Search..."
                placeholderTextColor="#718093"
                onChangeText={text => this.setState({ email: text })}
              />
            </View>
          </View>
          <View style={{ marginTop: 10 }}>
            <Text style={styles.kategoriText}>Kategori</Text>
            <ScrollView style={styles.cardKategoriScroll} horizontal={true}>
              {kategori.map((e, i) => {
                return (
                  <TouchableOpacity style={styles.cardKategori} key={i}>
                    <Ionicons name={e.icon} size={36} color={"#FFFFFF"} />
                    <Text style={styles.cardKategoriText}>{e.name}</Text>
                  </TouchableOpacity>
                );
              })}
            </ScrollView>
          </View>
          <View style={{ marginTop: 10 }}>
            <Text style={styles.kategoriText}>Venue</Text>
            <ScrollView contentContainerStyle={styles.cardVenueScroll}>
              {dataVenue.map((e, i) => {
                return (
                  <TouchableOpacity
                    style={styles.cardVenue}
                    onPress={() => this.props.navigation.navigate("Venue")}
                    key={i}
                  >
                    <Image
                      source={Logo}
                      style={{
                        height: "100%",
                        width: "30%",
                        resizeMode: "contain",
                        backgroundColor: "#ffffff",
                        borderRadius: 16,
                        flexDirection: "row"
                      }}
                    />
                    <View
                      style={{
                        flexDirection: "column",
                        marginLeft: 10
                      }}
                    >
                      <Text
                        style={{
                          color: "#0A3D62",
                          fontSize: 20
                        }}
                      >
                        {e.nama}
                      </Text>
                      <Text style={{ color: "#0A3D62" }}>{e.alamat}</Text>
                      <Text style={{ color: "#0A3D62" }}>
                        {e.booked}x Dipinjam pada minggu ini
                      </Text>
                    </View>
                  </TouchableOpacity>
                );
              })}
            </ScrollView>
          </View>
        </ScrollView>
      </View>
    );
  }
}

const dataVenue = [
  {
    id: 1,
    nama: "Venue Voli",
    image: "",
    alamat: "Jalan H.R. Soebrantas, Pekanbaru",
    booked: 5
  },
  {
    id: 2,
    nama: "Venue Basket",
    image: "",
    alamat: "Jalan H.R. Soebrantas, Pekanbaru",
    booked: 5
  },
  {
    id: 3,
    nama: "Venue Futsal",
    image: "",
    alamat: "Jalan H.R. Soebrantas, Pekanbaru",
    booked: 5
  },
  {
    id: 4,
    nama: "Venue Tenis",
    image: "",
    alamat: "Jalan H.R. Soebrantas, Pekanbaru",
    booked: 5
  },
  {
    id: 5,
    nama: "Venue Soccer",
    image: "",
    alamat: "Jalan H.R. Soebrantas, Pekanbaru",
    booked: 5
  }
];

const kategori = [
  { name: "Voli", icon: "ios-baseball" },
  { name: "Basket", icon: "ios-basketball" },
  { name: "Futsal", icon: "ios-american-football" },
  { name: "Tenis", icon: "ios-tennisball" },
  { name: "Soccer", icon: "ios-football" }
];

const styles = StyleSheet.create({
  screen: {
    flex: 1,
    backgroundColor: "#ffffff"
  },
  container: {
    backgroundColor: "#ffffff"
  },
  searchRow: {
    alignItems: "center"
  },
  searchInput: {
    width: "90%",
    backgroundColor: "#F5F6FA",
    borderRadius: 25,
    height: 50,
    marginTop: 20,
    marginBottom: 10,
    justifyContent: "center",
    padding: 20,
    shadowColor: "black",
    shadowOpacity: 0.2,
    shadowRadius: 10,
    shadowOffset: { height: 4, width: 4 }
  },
  inputText: {
    height: 50,
    color: "#718093"
  },
  kategoriText: {
    color: "#0A3D62",
    marginLeft: 20
  },
  cardKategoriScroll: {
    marginLeft: 10
  },
  cardKategori: {
    flexDirection: "column",
    alignContent: "center",
    alignItems: "center",
    padding: 15,
    margin: 5,
    height: 100,
    width: 100,
    backgroundColor: "#273C75",
    borderRadius: 16,
    shadowColor: "black",
    shadowOpacity: 0.2,
    shadowRadius: 5,
    shadowOffset: { height: 4, width: 4 }
  },
  cardKategoriText: {
    color: "#FFFFFF",
    fontSize: 12,
    alignItems: "center",
    margin: 10
  },
  cardVenueScroll: {
    marginHorizontal: 10,
    alignItems: "center",
    flexDirection: "column"
  },
  cardVenue: {
    flexDirection: "row",
    margin: 5,
    padding: 5,
    height: 100,
    width: "100%",
    backgroundColor: "#F5F6FA",
    borderRadius: 16,
    shadowColor: "black",
    shadowOpacity: 0.2,
    shadowRadius: 5,
    shadowOffset: { height: 4, width: 4 }
  },
  cardVenueText: {
    color: "#FFFFFF",
    fontSize: 12,
    alignItems: "center",
    margin: 10
  }
});

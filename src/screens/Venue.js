import React, { Component } from "react";
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  Button,
  TouchableOpacity,
  Image,
  Alert,
  ScrollView
} from "react-native";
import Logo from "../images/sport.png";

class Venue extends Component {
  render() {
    return (
      <View style={styles.screen}>
        <ScrollView contentContainerStyle={styles.container}>
          <View
            style={{
              marginTop: 10,
              alignItems: "center",
              justifyContent: "center"
            }}
          >
            <Image
              style={{
                width: "90%",
                height: 250,
                backgroundColor: "black",
                borderRadius: 12,
                resizeMode: "contain"
              }}
              source={Logo}
            />
          </View>
          <View style={{ marginTop: 10 }}>
            <Text style={styles.kategoriText}>Jadwal Main Bareng</Text>
            <ScrollView contentContainerStyle={styles.cardVenueScroll}>
              {dataJadalMain.map((e, i) => {
                return (
                  <View style={styles.cardVenue} key={e.id}>
                    <Image
                      source={Logo}
                      style={{
                        height: "100%",
                        width: "50%",
                        resizeMode: "contain",
                        backgroundColor: "#ffffff",
                        borderRadius: 16,
                        flexDirection: "row"
                      }}
                    />
                    <View
                      style={{
                        flexDirection: "column",
                        marginLeft: 15
                      }}
                    >
                      <Text
                        style={{
                          color: "#0A3D62",
                          fontSize: 20
                        }}
                      >
                        {e.tanggal}
                      </Text>
                      <Text style={{ color: "#0A3D62", marginBottom: 5 }}>
                        Jumlah Pemain: {e.jumlahPemain}
                      </Text>
                      <Button
                        title="Press me"
                        color="#F6B93B"
                        onPress={() => this.props.navigation.navigate("Join")}
                      />
                    </View>
                  </View>
                );
              })}
            </ScrollView>
          </View>
        </ScrollView>
      </View>
    );
  }
}
export default Venue;

const dataJadalMain = [
  {
    id: 1,
    tanggal: "09/04/2020",
    jumlahPemain: 1
  },
  {
    id: 2,
    tanggal: "11/02/2020",
    jumlahPemain: 3
  },
  {
    id: 3,
    tanggal: "26/03/2020",
    jumlahPemain: 7
  },
  {
    id: 4,
    tanggal: "18/04/2020",
    jumlahPemain: 8
  },
  {
    id: 5,
    tanggal: "29/04/2020",
    jumlahPemain: 4
  },
  {
    id: 6,
    tanggal: "03/03/2020",
    jumlahPemain: 2
  },
  {
    id: 7,
    tanggal: "15/02/2020",
    jumlahPemain: 3
  }
];

const styles = StyleSheet.create({
  screen: {
    flex: 1,
    backgroundColor: "#ffffff"
  },
  container: {
    backgroundColor: "#ffffff"
  },
  searchRow: {
    alignItems: "center"
  },
  kategoriText: {
    color: "#0A3D62",
    marginLeft: 20
  },
  cardKategoriScroll: {
    marginLeft: 10
  },
  cardKategori: {
    flexDirection: "column",
    alignContent: "center",
    alignItems: "center",
    padding: 15,
    margin: 5,
    height: 100,
    width: 100,
    backgroundColor: "#273C75",
    borderRadius: 16,
    shadowColor: "black",
    shadowOpacity: 0.2,
    shadowRadius: 5,
    shadowOffset: { height: 4, width: 4 }
  },
  cardKategoriText: {
    color: "#FFFFFF",
    fontSize: 12,
    alignItems: "center",
    margin: 10
  },
  cardVenueScroll: {
    marginHorizontal: 10,
    alignItems: "center",
    flexDirection: "column"
  },
  cardVenue: {
    flexDirection: "row",
    margin: 5,
    padding: 5,
    height: 100,
    width: "100%",
    backgroundColor: "#F5F6FA",
    borderRadius: 16,
    shadowColor: "black",
    shadowOpacity: 0.2,
    shadowRadius: 5,
    shadowOffset: { height: 4, width: 4 }
  },
  cardVenueText: {
    color: "#FFFFFF",
    fontSize: 12,
    alignItems: "center",
    margin: 10
  }
});

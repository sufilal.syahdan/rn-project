import React from "react";
import {
  StyleSheet,
  Text,
  View,
  Button,
  Image,
  Alert,
  ScrollView
} from "react-native";
import Logo from "../images/sport.png";

export default class Join extends React.Component {
  render() {
    return (
      <View style={styles.screen}>
        <ScrollView contentContainerStyle={styles.container}>
          <View
            style={{
              marginTop: 10,
              alignItems: "center",
              justifyContent: "center"
            }}
          >
            <Image
              style={{
                width: "90%",
                height: 250,
                backgroundColor: "black",
                borderRadius: 12,
                resizeMode: "contain"
              }}
              source={Logo}
            />
          </View>
          <View style={{ marginTop: 10 }}>
            <Text style={styles.kategoriText}>Jadwal Main Bareng</Text>
            <ScrollView contentContainerStyle={styles.cardVenueScroll}>
              <View style={styles.cardVenue}>
                <Image
                  source={Logo}
                  style={{
                    height: "100%",
                    width: "40%",
                    resizeMode: "contain",
                    backgroundColor: "#ffffff",
                    borderRadius: 16,
                    flexDirection: "row"
                  }}
                />
                <View
                  style={{
                    flexDirection: "column",
                    marginLeft: 15
                  }}
                >
                  <Text
                    style={{
                      color: "#0A3D62",
                      fontSize: 20
                    }}
                  >
                    Venue Futsal Pekanbaru
                  </Text>
                  <Text style={{ color: "#0A3D62", marginBottom: 5 }}>
                    Jumlah Pemain: 6
                  </Text>
                  <Button
                    title="Join"
                    color="#F6B93B"
                    onPress={() =>
                      Alert.alert("Button with adjusted color pressed")
                    }
                  />
                </View>
              </View>
            </ScrollView>
          </View>
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  screen: {
    flex: 1,
    backgroundColor: "#ffffff"
  },
  container: {
    backgroundColor: "#ffffff"
  },
  searchRow: {
    alignItems: "center"
  },
  kategoriText: {
    color: "#0A3D62",
    marginLeft: 20
  },
  cardKategoriScroll: {
    marginLeft: 10
  },
  cardKategori: {
    flexDirection: "column",
    alignContent: "center",
    alignItems: "center",
    padding: 15,
    margin: 5,
    height: 100,
    width: 100,
    backgroundColor: "#273C75",
    borderRadius: 16,
    shadowColor: "black",
    shadowOpacity: 0.2,
    shadowRadius: 5,
    shadowOffset: { height: 4, width: 4 }
  },
  cardKategoriText: {
    color: "#FFFFFF",
    fontSize: 12,
    alignItems: "center",
    margin: 10
  },
  cardVenueScroll: {
    marginHorizontal: 10,
    alignItems: "center",
    flexDirection: "column"
  },
  cardVenue: {
    flexDirection: "row",
    margin: 5,
    padding: 5,
    height: 100,
    width: "100%",
    backgroundColor: "#F5F6FA",
    borderRadius: 16,
    shadowColor: "black",
    shadowOpacity: 0.2,
    shadowRadius: 5,
    shadowOffset: { height: 4, width: 4 }
  },
  cardVenueText: {
    color: "#FFFFFF",
    fontSize: 12,
    alignItems: "center",
    margin: 10
  }
});

import React, { Component } from "react";
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableOpacity,
  Image
} from "react-native";
import Logo from "../images/sport.png";
import StackActions from "@react-navigation/native";

class Login extends Component {
  handleLogin = () => {
    if (true) {
      _storeData();
      this.props.navigation.dispatch(StackActions.replace("Dashboard"));
    } else {
    }
  };

  _storeData = async () => {
    try {
      await AsyncStorage.setItem("@MySuperStore:key", "I like to save it.");
    } catch (error) {
      // Error saving data
    }
  };
  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.logo}>Main Bersama</Text>
        <Image source={Logo} style={{ height: "25%", width: "80%" }} />
        <View style={styles.inputView}>
          <TextInput
            style={styles.inputText}
            placeholder="Username"
            placeholderTextColor="#718093"
            onChangeText={text => {}}
          />
        </View>
        <View style={styles.inputView}>
          <TextInput
            secureTextEntry
            style={styles.inputText}
            placeholder="Password"
            placeholderTextColor="#718093"
            onChangeText={text => {}}
          />
        </View>
        <TouchableOpacity
          style={styles.loginBtn}
          onPress={() => this.props.navigation.navigate("Dashboard")}
        >
          <Text style={styles.loginText}>LOGIN</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.signupBtn}
          onPress={() => this.props.navigation.navigate("Register")}
        >
          <Text style={styles.loginText}>SIGN UP</Text>
        </TouchableOpacity>

        <TouchableOpacity>
          <Text style={styles.forgot}>Forgot Password?</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

export default Login;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#ffffff",
    alignItems: "center",
    justifyContent: "center"
  },
  logo: {
    fontWeight: "bold",
    fontSize: 40,
    color: "#fb5b5a",
    marginBottom: 5
  },
  inputView: {
    width: "80%",
    backgroundColor: "#F5F6FA",
    borderRadius: 25,
    height: 50,
    marginBottom: 20,
    justifyContent: "center",
    padding: 20,
    shadowColor: "black",
    shadowOpacity: 0.2,
    shadowRadius: 10,
    shadowOffset: { height: 4, width: 4 }
  },
  inputText: {
    height: 50,
    color: "#718093"
  },
  forgot: {
    color: "#718093",
    fontSize: 11
  },
  loginBtn: {
    width: "80%",
    backgroundColor: "#273C75",
    borderRadius: 25,
    height: 50,
    alignItems: "center",
    justifyContent: "center",
    marginTop: 40,
    marginBottom: 10
  },
  loginText: {
    color: "white"
  },
  signupBtn: {
    width: "80%",
    backgroundColor: "#F6B93B",
    borderRadius: 25,
    height: 50,
    alignItems: "center",
    justifyContent: "center",
    marginBottom: 10
  }
});
